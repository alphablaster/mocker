package mocker

type Mock struct {
	ID            string `json:"id" db:"id" binding:"required"`
	Name          string `json:"name" db:"name" binding:"required"`
	Description   string `json:"description" db:"description" binding:"required"`
	Status        int    `json:"status" db:"status"`
	ContentType   string `json:"content_type" db:"content_type" binding:"required"`
	RequestMethod string `json:"request_method" db:"request_method" binding:"required"`
	RoutePath     string `json:"route_path" db:"route_path" binding:"required"`
	BodyType      string `json:"body_type" db:"body_type" binding:"required"`
	BodyContent   string `json:"body_content" db:"body_content" binding:"required"`
	//ScriptType    string
	//Script        string
	//Active        bool
	//MockOrder     int
}
