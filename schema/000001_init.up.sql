-- migrate -path ./schema -database 'postgresql://admin:admin@localhost:5432/mocker_development?sslmode=disable' up

CREATE TABLE mocks (
    id             serial       not null unique,
    name           varchar(255) not null,
    description    text,
    status         integer,
    content_type   varchar(255) not null,
    request_method varchar(255) not null,
    route_path     varchar(255) not null,
    body_type      varchar(255) not null,
    body_content   text
);
