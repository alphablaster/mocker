package handler

import (
	"bitbucket.org/alphablaster/mocker"
	"fmt"
	"github.com/go-chi/chi"
	"html/template"
	"log"
	"net/http"
	"strconv"
)

func (h *Handler) indexMocks(writer http.ResponseWriter, _ *http.Request) {
	mocks, err := h.services.Mock.GetAll()
	if err != nil {
		fmt.Fprintf(writer, err.Error())
	}

	t, err := template.ParseFiles("templates/mocks/index.html", "templates/header.html", "templates/footer.html")

	if err != nil {
		fmt.Fprintf(writer, err.Error())
	}

	t.ExecuteTemplate(writer, "mocks/index", mocks)
}

func (h *Handler) newMock(writer http.ResponseWriter, _ *http.Request) {
	t, err := template.ParseFiles(
		"templates/mocks/new.html",
		"templates/mocks/fields.html",
		"templates/header.html",
		"templates/footer.html",
	)

	if err != nil {
		fmt.Fprintf(writer, err.Error())
	}

	mock := mocker.Mock{}

	t.ExecuteTemplate(writer, "mocks/new", mock)
}

func (h *Handler) createMock(writer http.ResponseWriter, request *http.Request) {
	name := request.FormValue("mockName")
	description := request.FormValue("mockDescription")
	requestMethod := request.FormValue("mockRequestMethod")
	status, _ := strconv.Atoi(request.FormValue("mockStatus"))
	routePath := request.FormValue("mockRoutePath")
	bodyType := request.FormValue("mockBodyType")
	contentType := request.FormValue("mockContentType")
	bodyContent := request.FormValue("mockBodyContent")

	mock := mocker.Mock{
		Name:          name,
		Description:   description,
		RequestMethod: requestMethod,
		Status:        status,
		RoutePath:     routePath,
		BodyType:      bodyType,
		ContentType:   contentType,
		BodyContent:   bodyContent,
	}

	err := h.services.Create(mock)

	if err != nil {
		fmt.Fprintf(writer, err.Error())
	}

	http.Redirect(writer, request, "/mocks", 302)
}

func (h *Handler) editMock(writer http.ResponseWriter, request *http.Request) {
	id, _ := strconv.Atoi(chi.URLParam(request, "ID"))
	mock, err := h.services.FindById(id)

	if err != nil {
		fmt.Fprintf(writer, err.Error())
	}

	t, err := template.ParseFiles(
		"templates/mocks/edit.html",
		"templates/mocks/fields.html",
		"templates/header.html",
		"templates/footer.html",
	)

	if err != nil {
		fmt.Fprintf(writer, err.Error())
	}

	t.ExecuteTemplate(writer, "mocks/edit", mock)
}

func (h *Handler) updateMock(writer http.ResponseWriter, request *http.Request) {
	id, _ := strconv.Atoi(chi.URLParam(request, "ID"))
	values := map[string]interface{}{
		"name":           request.FormValue("mockName"),
		"description":    request.FormValue("mockDescription"),
		"request_method": request.FormValue("mockRequestMethod"),
		"status":         request.FormValue("mockStatus"),
		"route_path":     request.FormValue("mockRoutePath"),
		"body_type":      request.FormValue("mockBodyType"),
		"content_type":   request.FormValue("mockContentType"),
		"body_content":   request.FormValue("mockBodyContent"),
	}
	err := h.services.Update(id, values)

	if err != nil {
		fmt.Fprintf(writer, err.Error())
	}

	http.Redirect(writer, request, "/mocks", 302)
}

func (h *Handler) deleteMock(writer http.ResponseWriter, request *http.Request) {
	id, _ := strconv.Atoi(chi.URLParam(request, "ID"))
	err := h.services.Delete(id)
	if err != nil {
		log.Fatal(err)
	}

	http.Redirect(writer, request, "/mocks", 302)
}

func (h *Handler) serveMock(writer http.ResponseWriter, request *http.Request) {
	requestMethod := request.Method
	routePath := chi.URLParam(request, "*")
	mock, err := h.services.Serve(requestMethod, routePath)

	if err != nil {
		//TODO: if not found document return http 404 Not Found error
		writer.WriteHeader(422)
		writer.Write([]byte(err.Error()))
		return
	}

	writer.Header().Set("Content-Type", mock.ContentType)
	writer.Write([]byte(mock.BodyContent))
}
