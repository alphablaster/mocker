package handler

import (
	"bitbucket.org/alphablaster/mocker/pkg/service"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"net/http"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	fs := http.FileServer(http.Dir("./assets"))
	r.Handle("/assets/*", http.StripPrefix("/assets", fs))

	r.Route("/mocks", func(r chi.Router) {
		r.Get("/", h.indexMocks)            // GET /mocks
		r.Get("/new", h.newMock)            // GET /mocks/new
		r.Post("/", h.createMock)           // POST /mocks
		r.Get("/{ID}/edit", h.editMock)     // GET /mocks/:id/edit
		r.Post("/{ID}", h.updateMock)       // POST /mocks/:id
		r.Get("/{ID}/delete", h.deleteMock) // DELETE /mocks/:id/delete
	})

	r.MethodFunc("GET", "/*", h.serveMock)
	r.MethodFunc("POST", "/*", h.serveMock)
	r.MethodFunc("PUT", "/*", h.serveMock)
	r.MethodFunc("PATCH", "/*", h.serveMock)
	r.MethodFunc("DELETE", "/*", h.serveMock)
	r.MethodFunc("OPTIONS", "/*", h.serveMock)
	r.MethodFunc("HEAD", "/*", h.serveMock)

	return r
}
