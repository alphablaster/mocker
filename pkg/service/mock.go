package service

import (
	"bitbucket.org/alphablaster/mocker"
	"bitbucket.org/alphablaster/mocker/pkg/repository"
)

type MockService struct {
	repo repository.Mock
}

func NewMockService(repo repository.Mock) *MockService {
	return &MockService{repo: repo}
}

func (s *MockService) GetAll() ([]mocker.Mock, error) {
	return s.repo.GetAll()
}

func (s *MockService) FindById(id int) (mocker.Mock, error) {
	return s.repo.FindById(id)
}

func (s *MockService) Create(mock mocker.Mock) error {
	return s.repo.Create(mock)
}

func (s *MockService) Update(id int, values map[string]interface{}) error {
	return s.repo.Update(id, values)
}

func (s *MockService) Delete(id int) error {
	return s.repo.Delete(id)
}

func (s *MockService) Serve(requestMethod string, routePath string) (mocker.Mock, error) {
	return s.repo.FindByServeParams(requestMethod, routePath)
}
