package service

import (
	"bitbucket.org/alphablaster/mocker"
	"bitbucket.org/alphablaster/mocker/pkg/repository"
)

type Mock interface {
	GetAll() ([]mocker.Mock, error)
	FindById(id int) (mocker.Mock, error)
	Serve(requestMethod string, routePath string) (mocker.Mock, error)
	Create(mock mocker.Mock) error
	Update(id int, values map[string]interface{}) error
	Delete(id int) error
}

type Service struct {
	Mock
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		Mock: NewMockService(repos.Mock),
	}
}
