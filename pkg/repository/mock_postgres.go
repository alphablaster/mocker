package repository

import (
	"bitbucket.org/alphablaster/mocker"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"strings"
)

type MockPostgres struct {
	db *sqlx.DB
}

func NewMockPostgres(db *sqlx.DB) *MockPostgres {
	return &MockPostgres{db: db}
}

func (r *MockPostgres) GetAll() ([]mocker.Mock, error) {
	var mocks []mocker.Mock
	query := fmt.Sprintf("SELECT * FROM %s", mocksTable)
	if err := r.db.Select(&mocks, query); err != nil {
		return nil, err
	}

	return mocks, nil
}

func (r *MockPostgres) FindById(id int) (mocker.Mock, error) {
	var mock mocker.Mock
	query := fmt.Sprintf("SELECT * FROM mocks WHERE id=%d", id)
	if err := r.db.Get(&mock, query); err != nil {
		return mock, err
	}

	return mock, nil
}

func (r *MockPostgres) Create(mock mocker.Mock) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	var id int
	createMockQueryStr := `INSERT INTO mocks (name, description, status, content_type, request_method, route_path,
		body_type, body_content) values ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id`

	row := tx.QueryRow(
		fmt.Sprintf(createMockQueryStr),
		mock.Name,
		mock.Description,
		mock.Status,
		mock.ContentType,
		mock.RequestMethod,
		mock.RoutePath,
		mock.BodyType,
		mock.BodyContent,
	)
	err = row.Scan(&id)
	if err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()

	return nil
}

func (r *MockPostgres) Update(id int, values map[string]interface{}) error {
	setValues := make([]string, 0)

	for k, v := range values {
		setValues = append(setValues, fmt.Sprintf("%s='%s'", k, v))
	}

	setQuery := strings.Join(setValues, ", ")
	query := fmt.Sprintf(`UPDATE mocks SET %s WHERE id = %d`, setQuery, id)
	logrus.Print(query)
	_, err := r.db.Exec(query)

	if err != nil {
		return err
	}

	return nil
}

func (r *MockPostgres) Delete(id int) error {
	query := fmt.Sprintf("DELETE FROM mocks WHERE id=%d", id)
	_, err := r.db.Exec(query)
	if err != nil {
		return err
	}

	return nil
}

func (r *MockPostgres) FindByServeParams(requestMethod string, routePath string) (mocker.Mock, error) {
	var mock mocker.Mock
	query := fmt.Sprintf("SELECT * FROM mocks WHERE request_method='%s' AND route_path='%s'", requestMethod, routePath)
	err := r.db.Get(&mock, query)

	if err != nil {
		return mock, err
	}

	return mock, err
}
