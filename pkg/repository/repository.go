package repository

import (
	"bitbucket.org/alphablaster/mocker"
	"github.com/jmoiron/sqlx"
)

type Mock interface {
	GetAll() ([]mocker.Mock, error)
	FindById(id int) (mocker.Mock, error)
	FindByServeParams(requestMethod string, routePath string) (mocker.Mock, error)
	Create(mock mocker.Mock) error
	Update(id int, values map[string]interface{}) error
	Delete(id int) error
}

type Repository struct {
	Mock
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Mock: NewMockPostgres(db),
	}
}
